
# Arithmetic Game
This is a simple arithmetic game for Unity. The player has to answer a series of random arithmetic questions. If the player's answer is correct, they get a new question. If their answer is incorrect, they are told that they are wrong and the question remains the same.

The game ends when the player has answered a certain number of questions correctly (configurable in the game settings). If the player answers all of the questions correctly, they win the game. Otherwise, they lose the game.

## How to play
1. Click the "Start" button to start the game.
2. Answer the arithmetic question that is displayed on the screen.
3. Click the "Check" button to check your answer.
4. If your answer is correct, you will get a new question.
5. If your answer is incorrect, you will be told that you are wrong and the question will remain the same.
6. Continue answering questions until you win or lose the game.
