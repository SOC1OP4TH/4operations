using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calculate : MonoBehaviour
{
    public Button check;
    public Button next;
    public Text firstNumber;
    public Text secondNumber;
    public Text playerInput;
    public InputField result;
    public Text operation;
    public Text info;
    int first;
    int second;
    float answer;
    int playerAnswer;

    char[] operations = new char[]{'+','-','*','/'};
    // Start is called before the first frame update
    void Start()
    {
     NextQuestion();
    }
    float calc(){
        if(operation.text == "+"){
            answer = first + second;
        }
        else if(operation.text == "-"){
            answer = first - second;
        }
        else if(operation.text == "*"){
            answer = first * second;
        }
        else if(operation.text == "/"){
            answer = Mathf.Round(first / second);
        }
        return answer;
    }
     public void checkAnswer(){
        answer = calc();
        playerAnswer = int.Parse(playerInput.text);
        if(playerAnswer == answer){
            info.text = "Correct!";
            Invoke("NextQuestion",2);
            
        }
        else{
            info.text = "Wrong!";
        }
    }
   public void NextQuestion(){

        
        info.text = "";
        result.text = "";        
        first= Random.Range(1,10);
        second= Random.Range(1,10);
        firstNumber.text = first.ToString();
        secondNumber.text = second.ToString();
        operation.text = operations[Random.Range(0,4)].ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return)){
            checkAnswer();
        }

    }
}
